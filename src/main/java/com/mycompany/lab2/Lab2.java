/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Lab2 {
    
    static char[][] table = {{'-','-','-'}, {'-','-','-'}, {'-','-','-'},};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
    
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWinner()) {
                printTable();
                printWinner();
                break;
            } else if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to OX Program");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col : ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }

    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWinner() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println(currentPlayer + " Win");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) return true;
        }
        return false;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
       return true;
    }

    private static void printDraw() {
        System.out.println("It's a draw");
    }

}
